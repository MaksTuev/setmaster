package com.two_man.setmaster.ui.base;

public interface HasPresenter {
    BasePresenter getPresenter();
    void initPresenter();
}
