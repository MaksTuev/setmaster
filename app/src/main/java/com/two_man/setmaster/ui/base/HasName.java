package com.two_man.setmaster.ui.base;

public interface HasName {
    String getName();
}
