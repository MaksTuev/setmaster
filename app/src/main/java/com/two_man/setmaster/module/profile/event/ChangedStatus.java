package com.two_man.setmaster.module.profile.event;

/**
 *
 */
public enum  ChangedStatus {
    UPDATED,
    DELETED
}
